FROM node:12-alpine AS builder

WORKDIR /app

COPY package.json package-lock.json ./

RUN npm install

COPY . .

RUN npm run build

FROM nginx:alpine

ENV NGINX_PORT=5000

COPY --from=builder /app/dist/* /usr/share/nginx/html